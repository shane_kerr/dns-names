"""
Example code showing various ways to convert DNS names to other formats.
"""

import base64
import readline  # noqa: F401, pylint: disable=unused-import
import traceback

# We use the idna module from PyPI instead of the built-in IDNA
# support in Python. As explained in the documentation:
#
#   This module implements RFC 3490 (Internationalized Domain Names in
#   Applications) and RFC 3492 (Nameprep: A Stringprep Profile for
#   Internationalized Domain Names (IDN)). It builds upon the punycode
#   encoding and stringprep.
#
#   If you need the IDNA 2008 standard from RFC 5891 and RFC 5895, use
#   the third-party idna module.
#
#   https://docs.python.org/3/library/codecs.html#module-encodings.idna
import idna


class DNSParsingException(Exception):
    """Parent exception for any DNS parsing problems"""


class EmptyLabel(DNSParsingException):
    """Empty label either at beginning or middle of a DNS name"""


class LabelTooLong(DNSParsingException):
    """DNS label more than 63 bytes long"""


class NameTooLong(DNSParsingException):
    """DNS name more than 255 bytes long in wire format"""


class TrailingBackslash(DNSParsingException):
    """DNS name where the presentation format ends with a backslash"""


class BadOctet(DNSParsingException):
    """DNS name with an escaped 3-digit value more than 255"""


class NameHasNUL(Exception):
    """Invalid attempt to convert a DNS name with a NUL/0x00 character"""


def parse_name(name: str) -> (list[bytes], list[str]):
    """
    Parse a presentation-format DNS name, returning a list of bytes.

    Handles quoted characters and octets, as well as converting any
    Unicode to Punycode.
    """
    # pylint: disable=too-many-branches

    # Handle the root zone as a special case.
    if name == ".":
        return [], []
    # Remove the trailing '.', if any.
    if name[-1:] == ".":
        name = name[:-1]

    labels = []
    binary_labels = []
    label_chars = []
    ofs = 0
    while True:
        # Either end of string or '.' indicates end of label
        if (ofs >= len(name)) or (name[ofs] == "."):
            # Convert our characters to a string, then decode it.
            label = "".join(label_chars)
            if label == "":
                raise EmptyLabel()
            if len(label) > 63:
                raise LabelTooLong()
            label_chars = []
            labels.append(label)
            try:
                binary_labels.append(idna.encode(label.lower()))
            except UnicodeError:
                binary_labels.append(bytes(label, "latin-1"))

            # If we are at the end of the name, make sure that it is
            # not too long and return it.
            if ofs >= len(name):
                len_check = sum(len(check) for check in binary_labels)
                len_check += len(binary_labels)
                if len_check > 254:
                    raise NameTooLong()
                return binary_labels, labels
            ofs += 1

        # If not at end of label, handle quoted characters
        elif name[ofs] == "\\":
            # Backslash as the last character in a name is undefined
            if ofs == len(name) - 1:
                raise TrailingBackslash()

            # Pull out up to 3 characters after the backslash, and see
            # if they are all digits. If so, convert to a number and
            # verify that it is a valid octet. If valid, use the octet.
            val_str = name[ofs + 1 : ofs + 4]
            if val_str.isdigit():
                val = int(val_str)
                if val > 255:
                    raise BadOctet()
                label_chars.append(chr(val))
                ofs += 4

            # If not an encoded octet, it is a simple quoted character.
            else:
                label_chars.append(name[ofs + 1])
                ofs += 2

        # Otherwise it is just a normal character
        else:
            label_chars.append(name[ofs])
            ofs += 1


def presentation(labels: str) -> str:
    """
    Return our canonical version of a DNS name.
    """
    presentation_labels = []
    for label in labels:
        label_parts = []
        for char in label:
            if char in '"$().;@':
                label_parts.append(f"\\{char}")
            elif (char in "\\ ") or (
                (ord(char) < 256) and not char.isprintable()
            ):
                label_parts.append(f"\\{ord(char):03}")
            else:
                label_parts.append(char)

        presentation_labels.append("".join(label_parts).lower())
    return ".".join(presentation_labels)


def name_to_sortable(labels: list[bytes]) -> bytes:
    """
    Convert the name to a sortable bytes value, with NUL not allowed.
    """
    for label in labels:
        if b"\x00" in label:
            raise NameHasNUL()
    return b"\x00".join(reversed([label.lower() for label in labels]))


def name_to_twobytes(labels: list[bytes]) -> bytes:
    """
    Convert the name to a sortable bytes value, with NUL allowed.
    """
    converted_labels = []
    for label in labels:
        converted_label = []
        for octet in label:
            converted_label.append(1)
            converted_label.append(bytes([octet]).lower()[0])
        converted_labels.append(bytes(converted_label))
    return b"\x00".join(reversed(converted_labels))


def name_to_hex(labels: list[bytes]) -> str:
    """
    Convert the name to a sortable hex value.
    """
    converted_labels = []
    for label in labels:
        converted_labels.append(
            "".join([f"{octet:02x}" for octet in label.lower()])
        )
    return ".".join(reversed(converted_labels))


_BASE64_ALT = {
    "A": "0",
    "B": "1",
    "C": "2",
    "D": "3",
    "E": "4",
    "F": "5",
    "G": "6",
    "H": "7",
    "I": "8",
    "J": "9",
    "K": "A",
    "L": "B",
    "M": "C",
    "N": "D",
    "O": "E",
    "P": "F",
    "Q": "G",
    "R": "H",
    "S": "I",
    "T": "J",
    "U": "K",
    "V": "L",
    "W": "M",
    "X": "N",
    "Y": "O",
    "Z": "P",
    "a": "Q",
    "b": "R",
    "c": "S",
    "d": "T",
    "e": "U",
    "f": "V",
    "g": "W",
    "h": "X",
    "i": "Y",
    "j": "Z",
    "k": "a",
    "l": "b",
    "m": "c",
    "n": "d",
    "o": "e",
    "p": "f",
    "q": "g",
    "r": "h",
    "s": "i",
    "t": "j",
    "u": "k",
    "v": "l",
    "w": "m",
    "x": "n",
    "y": "o",
    "z": "p",
    "0": "q",
    "1": "r",
    "2": "s",
    "3": "t",
    "4": "u",
    "5": "v",
    "6": "w",
    "7": "x",
    "8": "y",
    "9": "z",
    "+": "{",
    "/": "}",
    "=": "/",
}


def name_to_base64alt(labels: list[bytes]) -> str:
    """
    Convert the name to a sortable Base64-style value.
    """
    converted_labels = []
    for label in labels:
        std_base64_label = base64.b64encode(label.lower()).decode("ascii")
        converted_label = [_BASE64_ALT[ch] for ch in std_base64_label]
        converted_labels.append("".join(converted_label))
    return ".".join(reversed(converted_labels))


A85CHARS = [bytes((i,)) for i in range(33, 118)]
A85CHARS2 = [(a + b) for a in A85CHARS for b in A85CHARS]


def name_to_ascii85(labels: list[bytes]) -> str:
    """
    Convert the name to a sortable Ascii85 value.
    """
    converted_labels = []
    for label in labels:
        # It's bad form to invoke a private function directly, but
        # there is no published way to disable converting a string of
        # zero to 'z'.
        # pylint: disable=protected-access
        ascii85_label = base64._85encode(label.lower(), A85CHARS, A85CHARS2)
        converted_labels.append(ascii85_label.decode("ascii"))
    return " ".join(reversed(converted_labels))


def main():
    """
    Main program. Read DNS names in a REPL.
    """
    while True:
        try:
            line = input("dns> ")
        except EOFError:
            line = ""
        if line == "":
            break
        try:
            binary_labels, labels = parse_name(line)
            print(f"parsed: {binary_labels}, {labels}")
            pres = presentation(labels)
            print(f"presentation: {pres}")
            try:
                sortable = name_to_sortable(binary_labels)
                print(f"sortable: {sortable}")
            except NameHasNUL:
                print("sortable: not possible")
            twobytes = name_to_twobytes(binary_labels)
            print(f"two bytes: {twobytes}")
            hex_encoded = name_to_hex(binary_labels)
            print(f"hex: {hex_encoded}")
            base64alt = name_to_base64alt(binary_labels)
            print(f"base64alt: {base64alt}")
            ascii85 = name_to_ascii85(binary_labels)
            print(f"ascii85: {ascii85}")
        except DNSParsingException as ex:
            traceback.print_exception(ex)
    print()


if __name__ == "__main__":
    main()
