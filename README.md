# What Is a DNS Name?

DNS names are made from DNS labels. Each DNS label can contain any
bytes and is between 1 and 63 bytes long.

In the DNS name `www.example.net`, the labels are `www`, `example`,
and `net`, in that order.

When encoded in _wire format_, each label has a byte indicating how
long it is. The final label is indicated with a 0 length. The name
cannot be more than 255 bytes when encoded.

This approach means there are no limits on what characters can appear
in a DNS label, and therefore there are no limits on what characters
can appear in a DNS name.

DNS names also have a _presentation format_. This is an ASCII
representation, suitable for DNS zone files and anywhere else a
printable version of names is needed.

In presentation format, any byte in a label can be escaped. This is
done by putting a backslash (`\`) character in front of the escaped
character. The backslash character itself must be escaped; any
character may be. To represent non-ASCII characters you use three
decimal digits, like this: `\003`.

| Presentation Format  | Wire Format                       |
|----------------------|-----------------------------------|
| example              | `\x07example\x00`                 |
| test.net             | `\x04test\x03net\x00`             |
| .                    | `\x00`                            |
| \000.compact.test    | `\x01\x00\x07compact\x04test\x00` |

# Relative Versus Absolute Names

It is sometimes possible to use only part of a DNS name, depending on
context. At many organizations computers are configured to
automatically add a suffix when looking up names. This is called a
_relative name_.

For example, if you work at `bigheartlessbank.example` and want to go
to `orphanfunds.bigheartlessbank.example` to foreclose on a loan, you
would be able to just use `orphanfunds` by itself and your web browser
would end up at the correct site.

If you want to specify that adding this suffix is not desired, you can
use an _absolute name_ (also called a "Fully Qualified Domain Name",
or FQDN). This is done by writing the name with a trailing dot, like
this: `bigheartlessbank.example.`.

There is no way to indicate that a name is relative. That is, if there
is no trailing dot, a name might be relative or absolute.

There are no relative names in wire format.

# Internationalized Domain Names (IDN)

DNS names with non-ASCII Unicode characters are encoded using
Punycode. In Punycode a label will be converted to an ASCII version,
starting with `xn--`. For example, `moon-🚀-trip` will convert to
`xn--moon--trip-w876i`.

In the DNS wire format, only the Punycode version is used.

In the DNS presentation format, either format may be used.

# Issues with DNS Wire and Presentation Formats

Neither the DNS wire format nor the DNS presentation format provide an
unambiguous version of a name, since bytes in DNS names that are ASCII
characters are case-insensitive. That is, `some.TEST` is the same as
`SOME.test`.

The DNS presentation format creates additional ways to represent the
same name. For example, `foo` can also be written as `\foo` or as
`\146oo`. IDN support makes this even more complex, since either the
text or Punycode version can be used. For example, `🪙` might also be
written as `xn--009h`, `xn-\-009h`, or `xn-\045009h`.

Neither the DNS wire format nor the DNS presentation format sort very
well. Being able to sort properly is always nice, useful for finding
the zone which best matches a query, and also necessary for DNSSEC
ordering.  Here is an example set of sorted names from
[RFC 4034](https://www.rfc-editor.org/rfc/rfc4034#section-6.1):

```
             example
             a.example
             yljkjljk.a.example
             Z.a.example
             zABC.a.EXAMPLE
             z.example
             \001.z.example
             *.z.example
             \200.z.example
```

The name `a.example` comes before `example` in both formats ("a"
before "e" in presentation format, and `\x01` before `\x07` in wire
format).

# How Can We Best Display a DNS Name?

For displaying names to humans, probably the best approach is to
define a canonical version and use that everywhere. Here are a set of
rules that should be reasonable:

1. Always use absolute names (FQDN).
2. If valid Unicode, process as Unicode. Otherwise, process as bytes.
3. Convert to lowercase.
4. Pick a specific set of characters to escape with a simple
   backslash.
5. Pick a specific set of characters to escape with a numeric
   encoding.

For characters to escape with a simple backslash, here is a proposed
set:

* `"` (double quote) - starts/ends a quoted string in zone files.
* `$` (dollar sign) - used for control entries like `$INCLUDE` or
  `$ORIGIN`, which are used in zone files.
* `(` (left parenthesis) - used for grouping in zone files.
* `)` (right parenthesis) - used for grouping in zone files.
* `.` (dot) - the label separator.
* `;` (semicolon) - starts a comment in zone files.
* `@` (at sign) - substituted for the origin in zone files.

For characters to escape with numeric encoding, here is a proposed
set:

* ` ` (space) - separates tokens in a zone file. Use numeric encoding
  rather than `\ ` to avoid confusing people looking at the name.
* `\` (backslash) - the escape character itself. Use numeric encoding
  rather than `\\` to avoid confusing people who might have a hard
  time counting runs of `\\`.

In addition, any non-printable single-octet character must use numeric
encoding.

# How Can We Best Store a DNS Name for Lookup?

Key/value databases and relational databases both allow a lookup using
exact key or next/previous keys. Lookup is normally done using a
simple string comparison.

In order to convert a name into a format that can be compared easily
in DNS ordering:

1. Convert to Punycode, if possible.
2. Convert to lowercase.
3. Reverse the order of the labels.

So, `www.example.com` becomes `com.example.www`.

At this point, we are not finished. We have two options on how to
proceed.

## Names Never Have a NUL Character

If we know that we will never have a NUL character (character 0x00) in
a name, we can replace the `.` with a NUL character, and then we are
done. So, `www.example.com` becomes `com\x00example\x00www`.

This replacement ensures that `www-example.com` will come after
`www.example.com`, even though a `-` sorts before `.` in the ASCII
character set. This applies for all characters with a value less than
that of a `.`.

This approach is simple and fast, and if you can be sure that a NUL
will never be in a name, then it is probably the best.

## Names Might Have a NUL Character

If it _is_ possible to have a NUL character (character 0x00) in a name
then it becomes more difficult, since the NUL character is a possible
value. Ideally we would be able to add a new character with a value
less than all other characters, but that would mean there are 257
characters.

### Double-Wide Encoding

One approach is to use two bytes for every one byte in the name. In
this approach, `bar.example` would become
`\x01e\x01x\x01a\x01m\x01p\x01l\x01e\x00\x01b\x01a\x01r`. The `.` is
still replaced with a NUL character, but every other character has a
SOH/0x01 character in front of it.

### Hex-Encoded Format

If we are going to use two encoded bytes for every non-encoded byte in
the name, we can instead convert the bytes to hex and have a printable
version that is _somewhat_ human readable. So, `example.org` would
first get reversed to `org.example`, but then converted to hex as
`6f7267.6578616d706c65`. Since the `.` comes before any number or
letter, we can leave it as the separator.

### Modified Base64 Format

An alternate scheme to encode the bytes can be chosen, such as Base64,
which encodes every 3 bytes into 4 printable characters. The standard
Base64 encoding cannot be used, since it does not preserve the same
sorting order, but an alternate alphabet can be used which does. By
choosing the correct alphabet, we can also continue to use `.` as the
separator.

### Tweaked Ascii85

Ascii85 could also be used, which encodes 4 bytes into 5 printable
characters, and which preserves sort order. It must be modified to
never use all-zero compression, and ` ` used to replace `.` as the
separator character between labels since `.` is part of the alphabet.

### Example Encoding

These are how the various encoding approaches look:

| Presentation Format  | Double-Wide Encoding                                                      | Hex-Encoded Format           | Modified Base64              | Tweaked Ascii85      |
|----------------------|---------------------------------------------------------------------------|------------------------------|------------------------------|----------------------|
| example              | `\x01e\x01x\x01a\x01m\x01p\x01l\x01e`                                     | `6578616d706c65`             | `PNXXRN1iPG//`               | `AU%X#E,9(`          |
| test.net             | `\x01n\x01e\x01t\x00\x01t\x01e\x01s\x01t`                                 | `6e6574.74657374`            | `RcLq.T6LpT0//`              | `DImn FCfN8`         |
| .                    |                                                                           |                              |                              |                      |
| \000.compact.test    | `\x01t\x01e\x01s\x01t\x00\x01c\x01o\x01m\x01p\x01a\x01c\x01t\x00\x01\x00` | `74657374.636f6d70616374.00` | `T6LpT0//.OszjS65ZT0//.00//` | `FCfN8 @rH4'@:OB !!` |

Note that while the double-wide encoding looks very big, that is
mostly because of the way it is written. It takes exactly the same
amount of space as the hex-encoded format.

In most cases when a NUL character is possible in a name, using the
hex version is probably the best approach, since it is somewhat
human-readable and contains only ASCII values.
